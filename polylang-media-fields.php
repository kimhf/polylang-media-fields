<?php
/*
Plugin Name: Polylang Media Fields
Plugin URI:
Description: Create language specific attachment custom fields.
Version: 0.6.0
Author: Kim Helge Frimanslund
Author URI:
License:
*/

Class PolylangMediaFieldsFilters extends PolylangMediaFields {
	function __construct() {
		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
    }

	function plugins_loaded() {
		global $polylang;
		if ( is_object( $polylang ) ) {

			$this->languagesList = $polylang->model->get_languages_list();
			$this->defaultLangSlug = pll_default_language();
			if ( ! is_admin() ) {
				$this->curLang = pll_current_language('slug');
			} else {
				if ( isset( $_POST['post_ID'] ) ) {
					$post_id = $_POST['post_ID'];
					$this->curLang = pll_get_post_language($post_id, 'slug');
				} else {
					$curLang = '';
					if ( ! empty( pll_current_language('slug') ) ) {
						$curLang = pll_current_language('slug');
					}
					if ( ! empty( $curLang ) ) {
						$this->curLang = $curLang;
					} else {
						$this->curLang = false;
					}
				}
			}

			add_filter( 'attachment_fields_to_edit', array( $this, 'polylang_attachment_fields_to_edit' ), 10, 2 );
			add_filter( 'attachment_fields_to_save', array( $this, 'polylang_attachment_fields_to_save' ), 10, 2 );

			add_filter( 'get_the_excerpt', array( $this, 'get_the_excerpt' ), 10, 2 );
			add_filter( 'get_the_lang_caption', array( $this, 'get_the_lang_caption' ), 10, 2 );
		}
	}

	function get_the_excerpt( $excerpt ) {
		global $post;
		if ( get_post_type( $post ) == 'attachment' && ! empty( $this->curLang ) && $this->curLang != $this->defaultLangSlug && ! is_admin() ) {
			$excerpt = get_post_meta( $post->ID, 'excerpt_'.$this->curLang, true );
		}
		return $excerpt;
	}

	/**
	 *
	 *
	 * @param $form_fields array, fields to include in attachment form
	 * @param $post object, attachment record in database
	 * @return $form_fields, modified form fields
	 */

	function polylang_attachment_fields_to_edit( $form_fields, $post ) {
		foreach ( $this->languagesList as $language ) {
			if ( $language->slug != $this->defaultLangSlug ) {
				$form_fields['excerpt_'.$language->slug] = array(
					'label' => __('Caption').' ( '.$language->flag.' '.$language->name.' )',
					'input' => 'textarea',
					'value' => get_post_meta( $post->ID, 'excerpt_'.$language->slug, true ),
					//'helps' => 'If provided, photo credit will be displayed',
				);
			}
		}
		return $form_fields;
	}

	/**
	 *
	 *
	 * @param $post array, the post data for database
	 * @param $attachment array, attachment fields from $_POST form
	 * @return $post array, modified post data
	 */

	function polylang_attachment_fields_to_save( $post, $attachment ) {
		foreach ( $this->languagesList as $language ) {
			if( isset( $attachment['excerpt_'.$language->slug] ) ) {
				update_post_meta( $post['ID'], 'excerpt_'.$language->slug, $attachment['excerpt_'.$language->slug] );
			}
		}
		return $post;
	}
}
new PolylangMediaFieldsFilters;

/**
 *
 *
 *
 *
 *
 */
Class PolylangMediaFields {

	var $languagesList;
	var $defaultLangSlug;
	var $curLang;

	function __construct() {
		$this->defaultLangSlug = pll_default_language();
    }

	function get_the_lang_caption( $post_id ) {
		$excerpt = '';
		$post = get_post( $post_id );
		if ( get_post_type( $post ) == 'attachment' && ! empty( $this->curLang ) && $this->curLang != $this->defaultLangSlug ) {
			$excerpt = get_post_meta( $post->ID, 'excerpt_'.$this->curLang, true );
		} else if ( get_post_type( $post ) == 'attachment' && ! empty( $this->curLang ) && $this->curLang == $this->defaultLangSlug ) {
			$excerpt = $post->post_excerpt;
		}
		return $excerpt;
	}

	public function set_curLang( $curLang ) {
		$this->curLang = $curLang;
	}
}


function get_the_lang_caption( $image_id, $args = array() ) {
	global $polylang;
	$post_excerpt = '';

	if ( is_object( $polylang ) ) {
		$PolylangMediaFields = new PolylangMediaFields;
		if ( empty( $args['curLang'] ) ){
			global $post;
			$post_id = $post->ID;
			$args['curLang'] = pll_get_post_language($post_id, 'slug');
		}

		if ( ! empty( $args['curLang'] ) ){
			$PolylangMediaFields->set_curLang( $args['curLang'] );
		}

		$post_excerpt = $PolylangMediaFields->get_the_lang_caption( $image_id );
	}
	return $post_excerpt;
}
